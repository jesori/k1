import org.junit.Test;

import javax.print.attribute.HashPrintServiceAttributeSet;
import java.util.Arrays;

public class ColorSort {

    enum Color {red, green, blue}

    ;

    public static void main(String[] param) {

    Color [] arr = {Color.blue, Color.red, Color.blue};
    reorder(arr);
        System.out.println(Arrays.toString(arr));

    }

    public static void reorder(Color[] balls) {
        int redIndx = 0;
        int blueIndex = balls.length;
        for (Color color : balls) {
            if (color == Color.red) {
                redIndx ++;
            }else if (color == Color.blue){
                blueIndex--;
            }
        }
        for (int i = 0; i < redIndx; i++) {
            balls[i] = Color.red;
        }
        for (int i = redIndx; i < blueIndex; i++) {
            balls[i] = Color.green;
        }
        for (int i = blueIndex; i < balls.length; i++) {
            balls[i] = Color.blue;
        }
    }
}

